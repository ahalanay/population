using DynamicalSystems
using GLMakie

function f(x,r0)
        r0/(1+(r0-1)*x)
end

function g(y)
    1/(1+y)
end

function gen_model(u,p,n)
    x, y = u
    c, r0 = p
    f0(x) = f(x,r0)
    xn = x*f(x,r0)*g(y)
    yn = c*x*f(x,r0)*(1-g(y))
    return SVector(xn, yn)
end

function hassel(u,p,n)
    x,y = u
    r,a,b,c = p
    xn = r*x*exp(-a*y)/(1+x)^b
    yn = c*x*(1-exp(-a*y))
    return SVector(xn,yn)
end

function metz(u,p,n)
  x,y = u
  a,b = p
  xn = a*x/(1+y^b)
  yn = a*x*y^b/(1+y^b)
  return SVector(xn,yn)
end

function simulation(model,tend,init_values,parameters)
    sistem=DiscreteDynamicalSystem(model, init_values,parameters)
    A = trajectory(sistem,tend)
    scatter(A[:,1],A[:,2],markersize=5)
end

function simulation!(model,tend,init_values,parameters)
    sistem=DiscreteDynamicalSystem(model, init_values,parameters)
    A = trajectory(sistem,tend)
    scatter!(A[:,1],A[:,2],markersize=5)
    current_figure()
end

function simulation_log(model,tend,init_values,parameters)
    sistem=DiscreteDynamicalSystem(model, init_values,parameters)
    A = trajectory(sistem,tend)
    scatter(A[:,1],A[:,2],markersize=2,axis=(xscale=log,yscale=log),label="($init_values)")
    axislegend()
    current_figure()
end

function simulation_log!(model,tend,init_values,parameters)
    sistem=DiscreteDynamicalSystem(model, init_values,parameters)
    A = trajectory(sistem,tend)
    scatter!(A[:,1],A[:,2],markersize=2,axis=(xscale=log,yscale=log))
    current_figure()
end

function simulation_metz_log(tend)
  b = 1
  a = 2
  x0 = 0.001
  y0 = 0.001
  xs(k)=x0+k*0.1
  ys(k)=y0+k*0.1 
  sistem=DiscreteDynamicalSystem(metz, [x0,y0],[a,b])
  A = trajectory(sistem,tend)
  scatter(A[:,1],A[:,2]; figure=(; resolution=(1900,1000),markersize=2),axis=(; xscale = log, yscale=log))
  for i ∈ 0:20 
    for j ∈ 0:20
      init_values=[xs(i),ys(j)]
      sistem=DiscreteDynamicalSystem(metz, init_values,[a,b])
      A = trajectory(sistem,tend)
      scatter!(A[:,1],A[:,2]; figure = (; resolution =(1900,1000),markersize=2),label="$init_values")
    end
  end
  Makie.save("metz.png",current_figure())
  current_figure()
end

function simulation_metz_bif_log(tend)
  b = 1
  a = 2
  x0 = 1.0
  y0 = 0.1
  simulation_log(metz,tend,[x0,y0],[a,b])
  as(k)=a-k*0.2
  while a>0
    simulation_log!(metz,tend,[x0,y0],[a,b])
    a-=0.2
  end
  current_figure()
end

function anim_metz_log(tend)
  b = 1
  a = 2
  x0 = 0.001
  y0 = 0.001
  xs(k)=x0+k*0.01
  ys(k)=y0+k*0.01 
  framerate=30
  sistem=DiscreteDynamicalSystem(metz, [x0,y0],[a,b])
  A = trajectory(sistem,tend)
  fig=scatter(A[:,1],A[:,2],markersize=2,axis=(xscale=log,yscale=log),figure=(resolution=(1900,1000),),label="(0,0)")
  #Legend(fig,"(0,0)")
  record(fig,"metz_anim.mp4",0:50; framerate=framerate) do i
    for j in 0:50
      init_values=[xs(i),ys(j)]
      sistem=DiscreteDynamicalSystem(metz, init_values,[a,b])
      A = trajectory(sistem,tend)
      scatter!(A[:,1],A[:,2],markersize=2,axis=(xscale=log,yscale=log),label="$init_values")
     # Legend(fig,"($i,$j)")
    end
  end
  Makie.save("metz.png",current_figure())
  current_figure()
end

