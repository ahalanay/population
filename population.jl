using DynamicalSystems
using GLMakie 
using InteractiveDynamics

function f(x,r0)
        r0*x/(1+(r0-1)*x)
end

function g(y)
    1/(1+y)
end

function gen_model(u,p,n)
    x, y = u
    b, r0 = p
    xn = f(x,r0)*g(y)
    yn = b*f(x,r0)*(1-g(y))
    return SVector(xn, yn)
end

function simulation_kot1(init_values,tend)
  b = 1.11
  r0=1.11
  x0, y0 = init_values
  xs(k)=x0+k*0.1
  ys(k)=y0+k*0.1 
  sistem=DiscreteDynamicalSystem(gen_model, [x0,y0],[b,r0])
  A = trajectory(sistem,tend)
  scatter(A[:,1],A[:,2]; figure=(; resolution=(1900,1000),markersize=2))
  for i ∈ 0:30 
     for j ∈ 0:30
       init_values=[xs(i),ys(j)]
       sistem=DiscreteDynamicalSystem(gen_model, init_values,[b,r0])
       A = trajectory(sistem,tend)
       scatter!(A[:,1],A[:,2]; figure = (; resolution =(1900,1000),markersize=2),label="$init_values")
     end
     println(i)
   end
   Makie.save("kot$x0,$y0.png",current_figure())
  current_figure()
end

function simulation_kot1_bif(pars,init_values,tend)
  b, r0 = pars
  x0, y0 = init_values
  bs(k)=b+k*0.1
  r0s(k)=r0+k*0.1 
  sistem=DiscreteDynamicalSystem(gen_model, [x0,y0],[b,r0])
  A = trajectory(sistem,tend)
  scatter(A[:,1],A[:,2]; figure=(; resolution=(1900,1000),markersize=2),axis=(title = "$b,$r0", ))
  for i ∈ 0:30 
     for j ∈ 0:30
       sistem=DiscreteDynamicalSystem(gen_model, init_values,[bs(i),r0s(j)])
       A = trajectory(sistem,tend)
       scatter!(A[:,1],A[:,2]; figure=(; resolution=(1900,1000),markersize=2),axis=(title = "$b,$r0", ))
     end
     println(i)
   end
   current_figure()
   # Makie.save("kot_bif$b,$r0.png",current_figure())
end

function simulation_kot1_equil(pars,tend,rmax)
  b, r0 = pars
  equil=[1/b,r0/(1+(r0-1)*(1/b))-1]
  println(equil)
  x0, y0 = equil
  xs(r,θ) = x0+r*cos(θ)
  ys(r,θ) = y0+r*sin(θ) 
  sistem=DiscreteDynamicalSystem(gen_model, [x0,y0],[b,r0])
  A = trajectory(sistem,tend)
  p=Figure(resolution=(1500,900))
  ax = Axis(p[1,1],title="Parameters are $b,$r0")
  # limits!(ax, -1,1,-1,1)
  scatter!(A[:,1],A[:,2], markersize=2)
  for r ∈ range(0,rmax,20) 
     for θ ∈ 0:0.1:2*π
      init_values = [xs(r,θ),ys(r,θ)]
       sistem=DiscreteDynamicalSystem(gen_model, init_values,[b,r0])
       A = trajectory(sistem,tend)
       scatter!(A[:,1],A[:,2], markersize=2)
     end
   end
   Makie.save("kot_equi$b,$r0.png",p)
   p
end 

function simulation_kot1_equil_graph(pars,tend,rmax)
  b, r0 = pars
  equil=SVector(1/b,r0/(1+(r0-1)*(1/b))-1)
  println(equil)
  x0, y0 = equil
  xs(r,θ) = x0+r*cos(θ)
  ys(r,θ) = y0+r*sin(θ) 
  sistem=DiscreteDynamicalSystem(gen_model, [x0,y0],[b,r0])
  A = trajectory(sistem,tend)
  l=length(A[:,1])
  p = Figure(resolution=(2500,1500))
  ax = Axis(p[1,1],title="Parameters are $b,$r0 xn")
  ay = Axis(p[2,1],title="Parameters are $b,$r0 yn")
  # limits!(ax, -1,1,-1,1)
  lines!(ax,1:l,A[:,1], markersize=4)
  lines!(ay,1:l,A[:,2], markersize=4)
  for r ∈ range(0,rmax,20) 
     for θ ∈ 0:0.1:2*π
      init_values = [xs(r,θ),ys(r,θ)]
       sistem=DiscreteDynamicalSystem(gen_model, init_values,[b,r0])
       A = trajectory(sistem,tend)
       lines!(ax, 1:l,A[:,1], markersize=4)
       lines!(ay,1:l,A[:,2], markersize=4)
     end
   end
   Makie.save("kot_equi_graph$b,$r0.png",p)
   p
end 

function interactive_kot1(pars)
  b,r0 = pars
  u1 = [1/b,r0/(1+(r0-1)*(1/b))-1]
  u2 = u1+[0.001,0.001]
  u3 = u1-[0.001,0.001]
  u4 = u1+[0.002,0.002]
  u5 = u1-[0.002,0.002]
  u6 = u1+[0.00,0.001]
  u0s =[u1]
  println(u0s)
  sistem=DiscreteDynamicalSystem(gen_model, u1,[b,r0])
  idxs = 1:2
  ps = Dict(
    1 => 0.1:0.1:3,
    2 => 0.1:0.1:5,
  )
  pnames = Dict(1 => "b", 2 => "r0")
  figure, obs = interactive_evolution(sistem, u0s; ps,idxs,tail=10000,pnames)

  # ds = Systems.towel() # 3D chaotic discrete system
  # u0s = [0.1ones(3) .+ 1e-3rand(3) for _ in 1:3]

  # figure, obs = interactive_evolution(
  #     ds, u0s; idxs = 1:3, tail = 100000,
  #  )
end